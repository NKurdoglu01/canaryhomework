//
//  CanaryHomework-Bridging-Header.h.h
//  CanaryHomework
//
//  Created by Naci Kurdoglu on 26/3/21.
//  Copyright © 2021 Michael Schroeder. All rights reserved.
//

#ifndef CanaryHomework_Bridging_Header_h_h
#define CanaryHomework_Bridging_Header_h_h

#import "APIClient.h"
#import "CoreDataController.h"
#import "Device+CoreDataProperties.h"
#import "Reading+CoreDataProperties.h"
#import "CoreDataController.h"

#endif /* CanaryHomework_Bridging_Header_h_h */

