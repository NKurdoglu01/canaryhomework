//
//  CanaryHomeworkTests.swift
//  CanaryHomeworkTests
//
//  Created by Naci Kurdoglu on 26/3/21.
//  Copyright © 2021 Michael Schroeder. All rights reserved.
//

import XCTest

class CanaryHomeworkTests: XCTestCase {
    
    var testDeviceId:String!
    
    
    override func setUp() {
        super.setUp()
        
        testDeviceId = "1"
        
    }

    override func tearDown() {
        testDeviceId = nil

        super.tearDown()
    }
    
    //  MARK: - CoreDataManager getAllDevices test
    func testCoreDataControllerGetDevices(){
        
        let expectation = XCTestExpectation(description: "Retrieving devices")

        CoreDataController.sharedCache().getAllDevices { (completed, success, array) in
            
            XCTAssertTrue(completed, "Request not completed")
            XCTAssertTrue(success, "Request not succesful")
            XCTAssertNotNil(array, "No data downloaded")
            XCTAssert(array is [Device], "Devices could not be retrieved")
            expectation.fulfill()
            
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    //  MARK: - CoreDataManager getDevices test for Device 1
    func testCoreDataControllerGetRedings(){
        
        let expectation = XCTestExpectation(description: "Retrieving readings")

        CoreDataController.sharedCache().getReadingsForDevice(self.testDeviceId) { (completed, success, array) in
            XCTAssertTrue(completed, "Request not completed")
            XCTAssertTrue(success, "Request not succesful")
            XCTAssertNotNil(array, "No data downloaded")
            XCTAssert(array is [Reading], "Readings could not be retrieved")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)

    }
    
    //  MARK: - ApiClient getAllDevices endpoint test
    func testAPIClientGetDevices(){
        
        let expectation = XCTestExpectation(description: "Downloading devices data")

        APIClient.shared()?.getDeviceWithCompletionBlock({ (success, array) in
            
            XCTAssertTrue(success, "Request not succesful")
            XCTAssertNotNil(array, "No data downloaded")
            XCTAssert(array is [NSDictionary], "Devices could not be retrieved")
                            
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 10.0)
        
    }
    
    //  MARK: - ApiClient getDeviceDetails endpoint test for Device 1
    func testAPIClientGetDeviceReadings() {
        
        let expectation = XCTestExpectation(description: "Downloading readings data")
        
        APIClient.shared()?.getDevice(self.testDeviceId, readingsWithCompletionBlock: { (success, array) in
            XCTAssertTrue(success, "Request not succesful")
            XCTAssertNotNil(array, "No data downloaded")
            XCTAssert(array is [NSDictionary], "Readings could not be retrieved")
                            
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 10.0)

    }

}
