//
//  DetailViewController.m
//  CanaryHomework
//
//  Created by Michael Schroeder on 9/24/19.
//  Copyright © 2019 Michael Schroeder. All rights reserved.
//

#import "DetailViewController.h"
#import "CoreDataController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *maxValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *minValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *avgValueLabel;

@property (nonatomic, retain) Device *device;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"About Device";
    
    
    [self setReadingsforDevice];
    
}

// Make Sure ViewController is deallocated to avoid memory leaks.
- (void)dealloc
{
    NSLog(@"Deinit DetailViewController called");
}

// MARK: - Assign the device to the ViewController.
- (void)SetDevice:(Device *)device {
    
    if (self.device != device) {
        self.device = device;
        [self setReadingsforDevice];
    }
}

// MARK: - If there is a device assigned to the ViewControlelr, try to retrieve device data and update the views.
- (void) setReadingsforDevice {

    if (self.device != nil) {
        
        self.title = self.device.name;
                
        [CoreDataController.sharedCache getReadingsForDevice:self.device.deviceID completionBlock:^(BOOL completed, BOOL success, NSArray * _Nonnull objects) {
            
            if (success) {
                NSArray *temperatures = [self.device.readings valueForKeyPath:@"value"];
                
                NSNumber *max = [temperatures valueForKeyPath: @"@max.intValue"];
                NSNumber *min = [temperatures valueForKeyPath: @"@min.intValue"];
                NSNumber *avg = [temperatures valueForKeyPath: @"@avg.intValue"];

                if (max != nil) {
                    self.maxValueLabel.text = max.description;
                    self.minValueLabel.text = min.description;
                    self.avgValueLabel.text = avg.description;
                }else{
                    self.maxValueLabel.text = @"No data";
                    self.minValueLabel.text = @"No data";
                    self.avgValueLabel.text = @"No data";
                }

            }else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: nil message:@"We could not retrieve device readings." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController dismissViewControllerAnimated:true completion: nil];
                }];
                [alertController addAction: ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }];
    }
}

@end
