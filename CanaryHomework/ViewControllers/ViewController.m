//
//  ViewController.m
//  CanaryHomework
//
//  Created by Michael Schroeder on 9/19/19.
//  Copyright © 2019 Michael Schroeder. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"
#import "CoreDataController.h"
#import "Device+CoreDataProperties.h"
#import "Device+ParserLogic.h"
#import "APIClient.h"

@interface ViewController ()

@property(nonatomic, retain) UITableView *tableView;
@property(nonatomic, retain) UILayoutGuide *safeArea;

@property (nonatomic, retain) NSArray * devices;


@end



@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Devices";
    // Do any additional setup after loading the view.
    self.safeArea = self.view.layoutMarginsGuide;
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupTableView];
    [self retrieveDevices];
}

- (void)setupTableView {
    self.tableView = [UITableView new];
    self.tableView.isAccessibilityElement = true;
    self.tableView.translatesAutoresizingMaskIntoConstraints = false;
    [self.view addSubview:self.tableView];
    [[self.tableView.topAnchor constraintEqualToAnchor:self.safeArea.topAnchor] setActive:true];
    [[self.tableView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor] setActive:true];
    [[self.tableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor] setActive:true];
    [[self.tableView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor] setActive:true];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    
}

// MARK: - Try to retrieve the devices and update the tableView accordingly. Display a popup in case devicec could not be retrieved.
- (void) retrieveDevices {
    
    [CoreDataController.sharedCache getAllDevices:^(BOOL completed, BOOL success, NSArray * _Nonnull objects) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (success) {
                self.devices = objects;
                [self.tableView reloadData];
            }else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: nil message:@"We could not retrieve devices." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController dismissViewControllerAnimated:true completion: nil];
                }];
                [alertController addAction: ok];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        });
    }];
}

#pragma mark - UITableView Data Source

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.isAccessibilityElement = true;
    cell.accessibilityIdentifier = @"DeviceCell";

    Device *device = [self.devices objectAtIndex:indexPath.row];
    
    cell.textLabel.text = device.name;
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.devices count];
}

#pragma mark UITableView Delegate 

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    DetailViewController *dc = [[UIStoryboard storyboardWithName:@"Details" bundle:nil] instantiateInitialViewController];
    Device *device = [self.devices objectAtIndex:indexPath.row];
    
    [dc SetDevice: device];
    [self.navigationController pushViewController:dc animated:YES];
}

@end
