//
//  CanaryHomeworkUITests.swift
//  CanaryHomeworkUITests
//
//  Created by Naci Kurdoglu on 27/3/21.
//  Copyright © 2021 Michael Schroeder. All rights reserved.
//

import XCTest

class CanaryHomeworkUITests: XCTestCase {

    var app:XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        self.app = XCUIApplication()
        app.activate()
        
    }
    
    override func tearDown() {
        app.terminate()
        
        super.tearDown()
        
    }
    
    //  MARK: - ViewController test for device list
    func testDevice1DetailsVC(){
        
        let device1 = app.tables/*@START_MENU_TOKEN@*/.staticTexts["Device 1"]/*[[".cells.staticTexts[\"Device 1\"]",".staticTexts[\"Device 1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        let _ = device1.waitForExistence(timeout: 5)
        device1.tap()
        
        let maxTemp = app.staticTexts["MaxTempLabel"]
        let minTemp = app.staticTexts["MinTempLabel"]
        let avgTemp = app.staticTexts["AvgTempLabel"]

        let _ = maxTemp.waitForExistence(timeout: 5)

        XCTAssertEqual(maxTemp.label, "90")
        XCTAssertEqual(minTemp.label, "10")
        XCTAssertEqual(avgTemp.label, "48.5")
        
    }
    
    //  MARK: - DetailViewCotroller test for device with no data history
    func testDevice3DetailsVC(){
        
        let device3 = app.tables.staticTexts["Device 3"]
        let _ = device3.waitForExistence(timeout: 5)
        device3.tap()
        
        let maxTemp = app.staticTexts["MaxTempLabel"]
        let minTemp = app.staticTexts["MinTempLabel"]
        let avgTemp = app.staticTexts["AvgTempLabel"]

        let _ = maxTemp.waitForExistence(timeout: 5)

        XCTAssertEqual(maxTemp.label, "No data")
        XCTAssertEqual(minTemp.label, "No data")
        XCTAssertEqual(avgTemp.label, "No data")

    }
    
    //  MARK: - DetailViewCotroller test for device with existing data history
    func testDevicesListVC() {
        
        let cell1 = app.tables.cells.element(boundBy: 2)
        let cell2 = app.tables.cells.element(boundBy: 1)
        let cell3 = app.tables.cells.element(boundBy: 0)

        let _ = cell1.waitForExistence(timeout: 5)
                
        XCTAssertEqual(cell1.label, "Device 1")
        XCTAssertEqual(cell2.label, "Device 2")
        XCTAssertEqual(cell3.label, "Device 3")
    }

    //  MARK: - Test for app launch performance
    func testLaunchPerformance() throws {
        if #available(iOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
